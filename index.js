function calculateRelativeStrengthIndex(candles, n, pastResults) {
  let gains = [];
  let losses = [];
  let rsis = [];
  let avgGains = [];
  let avgLosses = [];
  let i = 1;

  if (pastResults) {
    gains = pastResults.gains;
    losses = pastResults.losses;
    rsis = pastResults.rsis;
    avgGains = pastResults.avgGains;
    avgLosses = pastResults.avgLosses;
    i = candles.length - 1;
  }

  for (i; i < candles.length; i++) {
    if (candles[i].closingPrice > candles[i - 1].closingPrice) {
      gains[i - 1] = candles[i].closingPrice - candles[i - 1].closingPrice;
      losses[i - 1] = 0;
    } else {
      gains[i - 1] = 0;
      losses[i - 1] = candles[i - 1].closingPrice - candles[i].closingPrice;
    }

    if (i === n) {
      avgGains[i - n] = calculateSimpleMovingAverage(gains, n);
      avgLosses[i - n] = calculateSimpleMovingAverage(losses, n);
    }

    if (i > n) {
      avgGains[i - n] = (avgGains[i - n - 1] * (n - 1) + gains[i - 1]) / n;
      avgLosses[i - n] = (avgLosses[i - n - 1] * (n - 1) + losses[i - 1]) / n;
    }

    if (i >= n) {
      const rs = avgGains[i - n] / avgLosses[i - n];
      rsis.push(avgLosses[i - n] !== 0 ? 100 - (100 / (1 + rs)) : 100);
    }
  }

  return {
    gains,
    losses,
    rsis,
    avgGains,
    avgLosses,
  };
}

function getMax(values, property) {
  let max = 0;

  if (property) {
    values.forEach((value) => {
      max = value[property] > max ? value[property] : max;
    });
  } else {
    values.forEach((value) => {
      max = value > max ? value : max;
    });
  }

  return max;
}

function getMin(values, property) {
  let min = property ? values[0][property] : values[0];

  if (property) {
    values.forEach((value) => {
      min = value[property] < min ? value[property] : min;
    });
  } else {
    values.forEach((value) => {
      min = value < min ? value : min;
    });
  }

  return min;
}

function calculateStochasticOscillator(candles, lookBack, n, n2, pastResults) {
  let fastSOs = calculateFastStochasticOscillator(candles, lookBack, n, pastResults);

  let dFulls = [];
  let i = n2 - 1;
  if (pastResults) {
    dFulls = pastResults.dFulls;
    i = fastSOs.dFasts.length - 1;
  }

  for (i; i < fastSOs.dFasts.length; i++) {
    let sample = fastSOs.dFasts.slice(i - (n2 - 1), i + 1);
    dFulls.push(calculateMean(sample));
  }

  return {
    kFulls: fastSOs.dFasts,
    dFulls,
    dFasts: fastSOs.dFasts,
    kFasts: fastSOs.kFasts,
  };
}

function calculateFastStochasticOscillator(candles, lookBack, n, pastResults) {
  let kFasts = [];
  let dFasts = [];

  let i = lookBack - 1;

  if (pastResults) {
    kFasts = pastResults.kFasts;
    dFasts = pastResults.dFasts;
    i = candles.length - 1;
  }

  for (i; i < candles.length; i++) {
    let sample = candles.slice(i - (lookBack - 1), i + 1);
    let high = getMax(sample, 'highestPrice');
    let low = getMin(sample, 'lowestPrice');

    const kFast = high - low !== 0 ? (sample[sample.length - 1].closingPrice - low) / (high - low) * 100 : 50;

    kFasts.push(kFast);
  }

  let j = n - 1;
  if (pastResults) {
    j = kFasts.length - 1;
  }

  for (j; j < kFasts.length; j++) {
    let sample = kFasts.slice(j - (n - 1), j + 1);
    dFasts.push(calculateMean(sample));
  }

  return {
    kFasts,
    dFasts
  };
}

function calculateExponentialMovingAverage(values, n, property) {
  let emas = [];

  const alpha = 2 / (1 + n);
  const start = values.length === n ? 1 : n;

  if (property) {
    if (start === n) {
      emas.push(calculateMean(values.slice(0, start), property));
    } else {
      emas.push(values[0][property]);
    }
  } else {
    if (start === n) {
      emas.push(calculateMean(values.slice(0, start)));
    } else {
      emas.push(values[0]);
    }
  }

  for (let i = start; i < values.length; i++) {
    const value = property ? values[i][property] : values[i];
    emas.push((value * alpha) + emas[emas.length - 1] * (1 - alpha));
  }

  return emas;
}

function calculateTripleExponentialMovingAverage(values, n, property) {
  let emas = [];
  let emaOfEmas = [];
  let emaOfEmaOfEmas = [];

  let tema = [];

  emas = calculateExponentialMovingAverage(values, n, property);
  emaOfEmas = calculateExponentialMovingAverage(emas, n);
  emaOfEmaOfEmas = calculateExponentialMovingAverage(emaOfEmas, n);

  emas = emas.slice(-emaOfEmaOfEmas.length);
  emaOfEmas = emaOfEmas.slice(-emaOfEmaOfEmas.length);

  for (let i = 0; i < emaOfEmaOfEmas.length; i++) {
    tema.push(3 * emas[i] - 3 * emaOfEmas[i] + emaOfEmaOfEmas[i]);
  }

  return tema;
}

function getMovingAverageConvergenceDivergenceSignal(values, fast, slow, signal, property, pastResults) {
  let fastEmas;
  let slowEmas;
  let macds;
  let signals;
  let i;

  if (pastResults) {
    fastEmas = pastResults.fastEmas;
    slowEmas = pastResults.slowEmas;
    macds = pastResults.macds;
    signals = pastResults.signals;
    i = values.length - 1;
  } else {
    fastEmas = [calculateSimpleMovingAverage(values.slice(0, fast), fast, property ? property : null)];
    slowEmas = [calculateSimpleMovingAverage(values.slice(0, slow), slow, property ? property : null)];
    macds = [];
    signals = [];
    i = fast;
  }

  for (i; i < values.length; i++) {

    fastEmas.push((property ? values[i][property] : values[i]) * (2 / (fast + 1)) + fastEmas[fastEmas.length - 1] * (1 - (2 / (fast + 1))));

    if (i > slow - 1) {
      slowEmas.push((property ? values[i][property] : values[i]) * (2 / (slow + 1)) + slowEmas[slowEmas.length - 1] * (1 - (2 / (slow + 1))));
      macds.push(fastEmas[fastEmas.length - 1] - slowEmas[slowEmas.length - 1]);
    }
  }

  signals = calculateExponentialMovingAverage(macds, 9);

  const result = {
    fastEmas,
    slowEmas,
    macds,
    signals,
  };

  return result;
}

function calculateMean(values, property) {
  let sum = 0;

  for (let value of values) {
    if (property) {
      sum += value[property];
    } else {
      sum += value;
    }
  }

  return sum / values.length;
}

function calculateSimpleMovingAverage(values, n, property) {
  let smas = [];

  for (let i = n - 1; i < values.length; i++) {
    const sample = values.slice(i - n + 1, n);

    smas.push(calculateMean(sample, property));
  }

  return smas;
}

function calculateAwesomeOscillator(candles, pastResults) {
  let aos = [];
  let midPointPrices = [];
  let i = 0;

  if (pastResults) {
    aos = pastResults.aos;
    midPointPrices = pastResults.midPointPrices;
    i = candles.length - 1;
  }

  for (i; i < candles.length; i++) {
    const candle = candles[i];

    let midPointPrice = (Number(candle.highestPrice) + Number(candle.lowestPrice)) / 2;
    midPointPrices.push(midPointPrice);

    if (i >= 33) {
      let fast = calculateSimpleMovingAverage(midPointPrices.slice(-5), 5);
      let slow = calculateSimpleMovingAverage(midPointPrices.slice(-34), 34);

      aos.push(fast - slow);
    }
  }

  return {
    midPointPrices,
    aos,
  };
}

function calculateParabolicStopAndReverse(candles, startValue, increment, maxValue, pastResults) {
  let psar = [];

  let i;
  if (pastResults) {
    psar = pastResults;
    i = candles.length - 1;
  } else {
    psar[0] = {};
    psar[0].ep = candles[0].lowestPrice;
    psar[0].psar = candles[0].highestPrice;
    psar[0].trend = 'bearish';
    psar[0].accFactor = startValue;
    psar[0].eqInPsar = (psar[0].psar - psar[0].ep) * psar[0].accFactor;
    i = 1;
  }

  for (i; i < candles.length; i++) {
    psar[i] = {};

    if (psar[i - 1].trend === 'bearish') {
      psar[i].initialPsar = getMax([psar[i - 1].psar - psar[i - 1].eqInPsar, psar[i - 1].highestPrice, psar[i - 2] ?
        candles[i - 2].highestPrice : 0
      ]);
    } else if (psar[i - 1].trend === 'bullish') {
      psar[i].initialPsar = getMin([psar[i - 1].psar - psar[i - 1].eqInPsar, psar[i - 1].lowestPrice, psar[i - 2] ?
        candles[i - 2].lowestPrice : candles[i - 1].lowestPrice
      ]);
    }

    if (psar[i - 1].trend === 'bearish' && candles[i].highestPrice < psar[i].initialPsar) {
      psar[i].psar = psar[i].initialPsar;
    } else if (psar[i - 1].trend === 'bullish' && candles[i].lowestPrice > psar[i].initialPsar) {
      psar[i].psar = psar[i].initialPsar;
    } else if (psar[i - 1].trend === 'bearish' && candles[i].highestPrice >= psar[i].initialPsar) {
      psar[i].psar = psar[i - 1].ep;
    } else if (psar[i - 1].trend === 'bullish' && candles[i].lowestPrice <= psar[i].initialPsar) {
      psar[i].psar = psar[i - 1].ep;
    }

    psar[i].trend = psar[i].psar > candles[i].closingPrice ? 'bearish' : 'bullish';

    if (psar[i].trend === 'bearish') {
      psar[i].ep = psar[i - 1].ep < candles[i].lowestPrice ? psar[i - 1].ep : candles[i].lowestPrice;
    } else if (psar[i].trend === 'bullish') {
      psar[i].ep = psar[i - 1].ep > candles[i].highestPrice ? psar[i - 1].ep : candles[i].highestPrice;
    }

    if (psar[i].trend === psar[i - 1].trend && psar[i].ep !== psar[i - 1].ep && psar[i - 1].accFactor < maxValue) {
      psar[i].accFactor = psar[i - 1].accFactor + increment;
    } else if (psar[i].trend === psar[i - 1].trend && psar[i].ep === psar[i - 1].ep) {
      psar[i].accFactor = psar[i - 1].accFactor;
    } else if (psar[i].trend !== psar[i - 1].trend) {
      psar[i].accFactor = startValue;
    } else {
      psar[i].accFactor = maxValue;
    }

    psar[i].eqInPsar = (psar[i].psar - psar[i].ep) * psar[i].accFactor;
  }

  return psar;
}

function getTrueRange(candle, prevCandle) {
  return getMax([
    candle.highestPrice - candle.lowestPrice,
    Math.abs(candle.highestPrice - prevCandle.closingPrice),
    Math.abs(candle.lowestPrice - prevCandle.closingPrice)
  ]);
}

function calculateAverageTrueRange(candles, numPeriods) {
  let trueRange = candles[0].highestPrice - candles[0].lowestPrice;
  let atr = trueRange;
  const trueRanges = [trueRange];

  for (let i = 1; i < candles.length; i++) {
    trueRange = getTrueRange(candles[i], candles[i - 1]);
    trueRanges[i] = trueRange;

    if (i === numPeriods - 1) {
      atr = calculateSimpleMovingAverage(trueRanges, numPeriods);
    } else if (i > numPeriods - 1) {
      atr = (atr * 13 + trueRange) / 14;
    }
  }

  return atr;
}

function calculateAverageDirectionalMovementIndex(candles) {
  let values = [];
  for (let i = 1; i < candles.length; i++) {
    values[i - 1] = {};
    let upMove = candles[i].highestPrice - candles[i - 1].highestPrice;
    let downMove = candles[i - 1].lowestPrice - candles[i].lowestPrice;
    values[i - 1].plusDirectionalMovement = upMove > 0 && upMove > downMove ? upMove : 0;
    values[i - 1].minusDirectionalMovement = downMove > 0 && downMove > upMove ? downMove : 0;
    values[i - 1].trueRange = getTrueRange(candles[i], candles[i - 1]);

    if (i === 14) {
      values[i - 1].plusDirectionalMovements14 = 0;
      values[i - 1].minusDirectionalMovements14 = 0;
      values[i - 1].trueRanges14 = 0;
      for (let j = 0; j < 14; j++) {
        values[i - 1].plusDirectionalMovements14 += values[j].plusDirectionalMovement;
        values[i - 1].minusDirectionalMovements14 += values[j].minusDirectionalMovement;
        values[i - 1].trueRanges14 += values[j].trueRange;
      }
    } else if (i > 14) {
      values[i - 1].plusDirectionalMovements14 = values[i - 2].plusDirectionalMovements14 - (values[i - 2].plusDirectionalMovements14 / 14) + values[i - 1].plusDirectionalMovement;
      values[i - 1].minusDirectionalMovements14 = values[i - 2].minusDirectionalMovements14 - (values[i - 2].minusDirectionalMovements14 / 14) + values[i - 1].minusDirectionalMovement;
      values[i - 1].trueRanges14 = values[i - 2].trueRanges14 - (values[i - 2].trueRanges14 / 14) + values[i - 1].trueRange;
    }

    if (i >= 14) {
      values[i - 1].plusDirectionIndicator14 = 100 * values[i - 1].plusDirectionalMovements14 / values[i - 1].trueRanges14;
      values[i - 1].minusDirectionIndicator14 = 100 * values[i - 1].minusDirectionalMovements14 / values[i - 1].trueRanges14;
      values[i - 1].dI14Diff = Math.abs(values[i - 1].plusDirectionIndicator14 - values[i - 1].minusDirectionIndicator14);
      values[i - 1].dI14Sum = values[i - 1].plusDirectionIndicator14 + values[i - 1].minusDirectionIndicator14;
      values[i - 1].dx = 100 * values[i - 1].dI14Diff / values[i - 1].dI14Sum;
    }

    if (i === 27) {
      let sumDx = 0;
      for (let k = 0; k < 14; k++) {
        sumDx += values[k + 13].dx;
      }
      values[i - 1].adx = sumDx / 14;
    } else if (i > 27) {
      values[i - 1].adx = ((values[i - 2].adx * 13) + values[i - 1].dx) / 14;
    }
  }

  return values[values.length - 1];
}

function calculateMeanAbsoluteDeviation(values, mean) {
  let sum = 0;
  values.forEach(value => {
    sum += Math.abs(mean - value);
  });

  return sum / values.length;
}

function calculateTypicalPrice(highestPrice, lowestPrice, closingPrice) {
  return (highestPrice + lowestPrice + closingPrice) / 3;
}

function calculateCommodityChannelIndex(candles, n) {
  let typicalPrices = [];
  let ccis = [];

  for (let i = 0; i < candles.length; i++) {
    const typicalPrice = calculateTypicalPrice(candles[i].highestPrice, candles[i].lowestPrice, candles[i].closingPrice);
    typicalPrices.push(typicalPrice);

    if (i >= n - 1) {
      let sample = typicalPrices.slice(i - (n - 1), i + 1);
      let sma = calculateSimpleMovingAverage(sample, n);
      let meanAbsDev = calculateMeanAbsoluteDeviation(sample, sma);

      const cci = meanAbsDev !== 0 ? (sample[sample.length - 1] - sma) / (0.015 * meanAbsDev) : 0;

      ccis.push(cci);
    }
  }

  return ccis;
}

function calculateDonchianChannel(candles) {
  let results = {
    upperChannel: getMax(candles, 'highestPrice'),
    lowerChannel: getMin(candles, 'lowestPrice'),
  };
  results.middleChannel = (results.upperChannel + results.lowerChannel) / 2;
  return results;
}

function isBullishPin(candle) {
  let range = candle.highestPrice - candle.lowestPrice;
  let lowerShadow = Math.min(candle.openingPrice, candle.closingPrice) - candle.lowestPrice;
  return lowerShadow >= 0.667 * range;
}

function isBearishPin(candle) {
  let range = candle.highestPrice - candle.lowestPrice;
  let upperShadow = candle.highestPrice - Math.max(candle.openingPrice, candle.closingPrice);
  return upperShadow >= 0.667 * range;
}

function isBullishTweezerBottoms(candles) {
  if (candles.length !== 2) {
    return false;
  }

  if (candles[0].openingPrice > candles[0].closingPrice && candles[1].closingPrice > candles[1].openingPrice) {
    return isBullishPin({
      openingPrice: candles[0].openingPrice,
      closingPrice: candles[1].closingPrice,
      highestPrice: Math.max(candles[0].highestPrice, candles[1].highestPrice),
      lowestPrice: Math.min(candles[0].lowestPrice, candles[1].lowestPrice),
    });
  }
  return false;
}

function isBearishTweezerBottoms(candles) {
  if (candles.length !== 2) {
    return false;
  }

  if (candles[0].openingPrice < candles[0].closingPrice && candles[1].closingPrice < candles[1].openingPrice) {
    return isBearishPin({
      openingPrice: candles[0].openingPrice,
      closingPrice: candles[1].closingPrice,
      highestPrice: Math.max(candles[0].highestPrice, candles[1].highestPrice),
      lowestPrice: Math.min(candles[0].lowestPrice, candles[1].lowestPrice),
    });
  }
  return false;
}

function isOneWhiteSoldier(candles) {
  if (candles.length !== 2) {
    return false;
  }

  if (candles[1].openingPrice > candles[0].closingPrice) {
    if (candles[1].closingPrice > candles[0].openingPrice) {
      return true;
    }
  }

  return false;
}

function isOneBlackCrow(candles) {
  if (candles.length !== 2) {
    return false;
  }

  if (candles[1].openingPrice < candles[0].closingPrice) {
    if (candles[1].closingPrice < candles[0].openingPrice) {
      return true;
    }
  }

  return false;
}

function isMorningStar(candles) {
  if (candles.length !== 3) {
    return false;
  }

  // bearish first candle
  if (candles[0].closingPrice < candles[0].openingPrice) {
    // gap to second candle
    if (Math.max(candles[1].openingPrice, candles[1].closingPrice) < candles[0].closingPrice) {
      // gap to third candle
      if (candles[2].openingPrice > Math.max(candles[1].openingPrice, candles[1].closingPrice)) {
        // bullish third candle
        if (candles[2].closingPrice > candles[2].openingPrice) {
          // body of first candle greater than second candle
          if ((candles[0].openingPrice - candles[0].closingPrice) > Math.abs(candles[1].openingPrice - candles[1].closingPrice)) {
            // third candle closes above half the body of the first candle
            if (candles[2].closingPrice > ((candles[0].openingPrice - candles[0].closingPrice) / 2) + candles[0].closingPrice) {
              return true;
            }
          }
        }
      }
    }
  }

  return false;
}

function isEveningStar(candles) {
  if (candles.length !== 3) {
    return false;
  }

  // bullish first candle
  if (candles[0].closingPrice > candles[0].openingPrice) {
    // gap to second candle
    if (Math.min(candles[1].openingPrice, candles[1].closingPrice) > candles[0].closingPrice) {
      // gap to third candle
      if (candles[2].openingPrice < Math.min(candles[1].openingPrice, candles[1].closingPrice)) {
        // bearish third candle
        if (candles[2].closingPrice < candles[2].openingPrice) {
          // body of first candle greater than second candle
          if ((candles[0].closingPrice - candles[0].openingPrice) > Math.abs(candles[1].openingPrice - candles[1].closingPrice)) {
            // third candle closes below half the body of the first candle
            if (candles[2].closingPrice < (candles[0].closingPrice - (candles[0].closingPrice - candles[0].openingPrice) / 2)) {
              return true;
            }
          }
        }
      }
    }
  }

  return false;
}

function isBullishEngulfing(candles) {
  if (candles.length !== 2) {
    return false;
  }

  if (candles[0].openingPrice > candles[0].closingPrice) {
    if (candles[1].closingPrice > candles[0].openingPrice) {
      if (candles[1].closingPrice > candles[0].openingPrice && candles[1].openingPrice < candles[0].closingPrice) {
        return true;
      }
    }
  }

  return false;
}

function isBearishEngulfing(candles) {
  if (candles.length !== 2) {
    return false;
  }

  if (candles[0].openingPrice < candles[0].closingPrice) {
    if (candles[1].closingPrice < candles[0].openingPrice) {
      if (candles[1].openingPrice > candles[0].closingPrice && candles[1].closingPrice < candles[0].openingPrice) {
        return true;
      }
    }
  }

  return false;
}

function calculateMadridMovingAverageRibbon(candles, valueName) {
  const last10Candles = candles.slice(-10);
  const last10CandlesPop1 = candles.slice(-11, -1);
  const last200Candles = candles.slice(-200);

  let ema = calculateExponentialMovingAverage(last10Candles, 5, valueName);
  const ema5 = ema[ema.length - 1];

  ema = calculateExponentialMovingAverage(last10CandlesPop1, 5, valueName);
  const ema6Pop1 = ema[ema.length - 1];

  ema = calculateExponentialMovingAverage(last200Candles, 100, valueName);
  const ema100 = ema[ema.length - 1];

  const changeInEma5 = ema5 - ema6Pop1;

  if (changeInEma5 >= 0) {
    if (ema5 > ema100) {
      return 'lime';
    } else if (ema5 < ema100) {
      return 'green';
    }
  } else if (changeInEma5 < 0 && ema5 > ema100) {
    return 'maroon';
  } else if (changeInEma5 <= ema5 < ema100) {
    return 'red';
  }

  return 'grey';
}

function calculateForceIndex(candles, n) {
  const indices = [];

  for (let i = 1; i < candles.length; i++) {
    const forceIndex = (candles[i].closingPrice - candles[i - 1].closingPrice) * candles[i].volume;
    indices.push(forceIndex);
  }

  let emas = calculateExponentialMovingAverage(indices, n);

  return emas;
}

function calculateMoneyFlowIndex(candles, n) {
  let positiveMoneyFlows = [];
  let negativeMoneyFlows = [];
  let typicalPrices = [];
  let mfis = [];

  typicalPrices.push(calculateTypicalPrice(candles[0].highestPrice, candles[0].lowestPrice, candles[0].closingPrice));

  const sum = (accumulator, currVal) => accumulator + currVal;

  for (let i = 1; i < candles.length; i++) {
    const candle = candles[i];
    typicalPrices.push(calculateTypicalPrice(candle.highestPrice, candle.lowestPrice, candle.closingPrice));

    if (typicalPrices[i] > typicalPrices[i - 1]) {
      positiveMoneyFlows[i - 1] = typicalPrices[i] * candle.volume;
      negativeMoneyFlows[i - 1] = 0;
    } else {
      positiveMoneyFlows[i - 1] = 0;
      negativeMoneyFlows[i - 1] = typicalPrices[i] * candle.volume;
    }

    if (i >= n) {
      const moneyFlowRatio = positiveMoneyFlows.slice(i - n).reduce(sum) / negativeMoneyFlows.slice(i - n).reduce(sum);
      const moneyFlowIndex = moneyFlowRatio && 1 + moneyFlowRatio !== 0 ? 100 - 100 / (1 + moneyFlowRatio) : 50;
      mfis.push(moneyFlowIndex);
    }

  }

  return mfis;
}

function calculateVolumeWeightedAveragePrice(candles) {
  const values = [];
  const volumes = [];
  for (const candle of candles) {
    values.push(calculateTypicalPrice(candle.highestPrice, candle.lowestPrice, candle.closingPrice));
    volumes.push(candle.volume);
  }
  return calculateVolumeWeightedMovingAverage(values, volumes);
}

function calculateVolumeWeightedMovingAverage(values, volumes) {
  let sumVolTimesValue = 0;
  let sumVol = 0;
  for (let i = 0; i < values.length; i++) {
    sumVolTimesValue += values[i] * volumes[i];
    sumVol += volumes[i];
  }
  return sumVol === 0 ? sumVolTimesValue : sumVolTimesValue / sumVol;
}

function calculateRelativeVigorIndex(candles, n) {
  let indices = [];
  let volumes = [];
  let rvis = [];
  let signals = [];

  for (let i = 0; i < candles.length; i++) {
    const candle = candles[i];
    const denominator = candle.highestPrice - candle.lowestPrice;
    const index = denominator === 0 ? 0 : (candle.closingPrice - candle.openingPrice) / denominator;
    indices.push(index);
    volumes.push(candle.volume);

    if (i >= n - 1) {
      rvis.push(calculateSimpleMovingAverage(indices.slice(-n), n));
    }

    if (i >= n - 1 + 3) {
      signals.push(calculateVolumeWeightedMovingAverage(rvis.slice(-4), volumes.slice(-4)));
    }
  }

  return {
    rvis,
    signals
  };
}

function calculateOnBalanceVolume(candles) {
  let obv = [0];

  for (let i = 1; i < candles.length; i++) {
    if (candles[i].closingPrice > candles[i - 1].closingPrice) {
      obv.push(obv[i - 1] + candles[i].volume);
    } else if (candles[i].closingPrice < candles[i - 1].closingPrice) {
      obv.push(obv[i - 1] - candles[i].volume);
    } else {
      obv.push(obv[i - 1]);
    }
  }

  return obv;
}

function calculateRateOfChange(values, n, property) {
  let roc = [];

  for (let i = n; i < values.length; i++) {
    if (property) {
      roc.push((values[i][property] - values[i - n][property]) / values[i - n][property] * 100);
    } else {
      roc.push((values[i] - values[i - n]) / values[i - n] * 100);
    }
  }

  return roc;
}

function calculateTripleExponentialAverage(values, n, n2, property) {
  const ema = calculateExponentialMovingAverage(values, n, property);
  const emaOfEma = calculateExponentialMovingAverage(ema, n);
  const emaOfEmaOfEma = calculateExponentialMovingAverage(emaOfEma, n);
  const roc = calculateRateOfChange(emaOfEmaOfEma, 1);

  let signals = calculateExponentialMovingAverage(roc, n2);

  return {
    roc,
    signals,
  };
}

function calculateStandardDeviation(values, property) {
  let sum = 0;

  values.forEach((value) => {
    if (property) {
      sum += value[property];
    } else {
      sum += value;
    }
  });

  const average = sum / values.length;

  let accumulator = 0;
  for (let value of values) {
    if (property) {
      accumulator += Math.pow(value[property] - average, 2);
    } else {
      accumulator += Math.pow(value - average, 2);
    }
  }

  return Math.sqrt(accumulator / values.length);
}

function calculateBollingerBands(values, n, property) {
  let upper = [];
  let middle = [];
  let lower = [];

  for (let i = n - 1; i < values.length; i++) {
    middle.push(calculateMean(values.slice(i - n + 1, i + 1), property));

    const standardDeviation = calculateStandardDeviation(values.slice(i - n + 1, i + 1), property);
    upper.push(middle[middle.length - 1] + 2 * standardDeviation);
    lower.push(middle[middle.length - 1] - 2 * standardDeviation);
  }

  return {
    upper,
    middle,
    lower,
  };
}

module.exports = {
  calculateRelativeStrengthIndex,
  calculateStochasticOscillator,
  calculateExponentialMovingAverage,
  calculateTripleExponentialMovingAverage,
  getMovingAverageConvergenceDivergenceSignal,
  calculateAwesomeOscillator,
  calculateParabolicStopAndReverse,
  calculateAverageTrueRange,
  calculateAverageDirectionalMovementIndex,
  calculateCommodityChannelIndex,
  calculateSimpleMovingAverage,
  calculateFastStochasticOscillator,
  calculateDonchianChannel,
  isBullishPin,
  isBearishPin,
  isBullishTweezerBottoms,
  isBearishTweezerBottoms,
  isOneWhiteSoldier,
  isOneBlackCrow,
  isMorningStar,
  isEveningStar,
  isBullishEngulfing,
  isBearishEngulfing,
  calculateMadridMovingAverageRibbon,
  calculateForceIndex,
  calculateMoneyFlowIndex,
  calculateRelativeVigorIndex,
  calculateVolumeWeightedAveragePrice,
  calculateVolumeWeightedMovingAverage,
  calculateOnBalanceVolume,
  calculateRateOfChange,
  calculateTripleExponentialAverage,
  calculateStandardDeviation,
  calculateBollingerBands,
};