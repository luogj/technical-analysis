let myTaLib = require('../index');
let assert = require('assert');

describe('technical-analysis', function () {
  it('calculateEmaWithSeedEqualSma', function () {
    const candles = [{
      closingPrice: 102.32
    }, {
      closingPrice: 105.54
    }, {
      closingPrice: 106.59
    }, {
      closingPrice: 107.39
    }];

    let n = 3;
    let emas = myTaLib.calculateExponentialMovingAverage(candles, n, 'closingPrice');
    assert.equal(2, emas.length);
    assert.equal(106.10333333333334, emas[emas.length - 1]);
  });

  it('calculateEmaWithSeedEqualFirstValue', function () {
    const candles = [{
      closingPrice: 102.32
    }, {
      closingPrice: 105.54
    }, {
      closingPrice: 106.59
    }];

    let n = candles.length;
    let emas = myTaLib.calculateExponentialMovingAverage(candles, n, 'closingPrice');
    assert.equal(105.26, emas[emas.length - 1]);
  });

  it('calculateTema', function () {
    let candles = [{
      closingPrice: 102.32
    }, {
      closingPrice: 105.54
    }, {
      closingPrice: 106.59
    }, {
      closingPrice: 107.39
    }, {
      closingPrice: 107.45
    }, {
      closingPrice: 109.08
    }, {
      closingPrice: 109.07
    }, {
      closingPrice: 109.1
    }, {
      closingPrice: 106.09
    }, {
      closingPrice: 106.72
    }];
    let tema = myTaLib.calculateTripleExponentialMovingAverage(candles, 3, 'closingPrice');
    assert.equal(106.43188078703702, tema[tema.length - 1]);
  });

  it('calculateFastStochasticOscillator', function () {
    let candles = [{
      highestPrice: 127.01,
      lowestPrice: 125.36
    }, {
      highestPrice: 127.62,
      lowestPrice: 126.16
    }, {
      highestPrice: 126.59,
      lowestPrice: 124.93
    }, {
      highestPrice: 127.35,
      lowestPrice: 126.09
    }, {
      highestPrice: 128.17,
      lowestPrice: 126.82
    }, {
      highestPrice: 128.43,
      lowestPrice: 126.48
    }, {
      highestPrice: 127.37,
      lowestPrice: 126.03
    }, {
      highestPrice: 126.42,
      lowestPrice: 124.83
    }, {
      highestPrice: 126.90,
      lowestPrice: 126.39
    }, {
      highestPrice: 126.85,
      lowestPrice: 125.72
    }, {
      highestPrice: 125.65,
      lowestPrice: 124.56
    }, {
      highestPrice: 125.72,
      lowestPrice: 124.57
    }, {
      highestPrice: 127.16,
      lowestPrice: 125.07
    }, {
      highestPrice: 127.72,
      lowestPrice: 126.86,
      closingPrice: 127.29
    }, {
      highestPrice: 127.69,
      lowestPrice: 126.63,
      closingPrice: 127.18
    }, {
      highestPrice: 128.22,
      lowestPrice: 126.80,
      closingPrice: 128.01
    }];

    let result = myTaLib.calculateFastStochasticOscillator(candles, 14, 3);

    assert.equal(70.54263565891475, result.kFasts[result.kFasts.length - 3]);
    assert.equal(67.70025839793286, result.kFasts[result.kFasts.length - 2]);
    assert.equal(89.14728682170502, result.kFasts[result.kFasts.length - 1]);
    assert.equal(75.79672695951754, result.dFasts[result.dFasts.length - 1]);
  });

  it('calculateFastStochasticOscillatorWithPastResults', function () {
    let candles = [{
      highestPrice: 127.01,
      lowestPrice: 125.36
    }, {
      highestPrice: 127.62,
      lowestPrice: 126.16
    }, {
      highestPrice: 126.59,
      lowestPrice: 124.93
    }, {
      highestPrice: 127.35,
      lowestPrice: 126.09
    }, {
      highestPrice: 128.17,
      lowestPrice: 126.82
    }, {
      highestPrice: 128.43,
      lowestPrice: 126.48
    }, {
      highestPrice: 127.37,
      lowestPrice: 126.03
    }, {
      highestPrice: 126.42,
      lowestPrice: 124.83
    }, {
      highestPrice: 126.90,
      lowestPrice: 126.39
    }, {
      highestPrice: 126.85,
      lowestPrice: 125.72
    }, {
      highestPrice: 125.65,
      lowestPrice: 124.56
    }, {
      highestPrice: 125.72,
      lowestPrice: 124.57
    }, {
      highestPrice: 127.16,
      lowestPrice: 125.07
    }, {
      highestPrice: 127.72,
      lowestPrice: 126.86,
      closingPrice: 127.29
    }, {
      highestPrice: 127.69,
      lowestPrice: 126.63,
      closingPrice: 127.18
    }, {
      highestPrice: 128.22,
      lowestPrice: 126.80,
      closingPrice: 128.01
    }, {
      highestPrice: 128.27,
      lowestPrice: 126.71,
      closingPrice: 127.11
    }];

    const pastResults = {
      kFasts: [70.54263565891475, 67.70025839793286, 89.14728682170502],
      dFasts: [75.79672695951754]
    };

    let result = myTaLib.calculateFastStochasticOscillator(candles, 14, 3, pastResults);

    assert.equal(67.70025839793286, result.kFasts[result.kFasts.length - 3]);
    assert.equal(89.14728682170502, result.kFasts[result.kFasts.length - 2]);
    assert.equal(65.89147286821691, result.kFasts[result.kFasts.length - 1]);
    assert.equal(75.79672695951754, result.dFasts[result.dFasts.length - 2]);
    assert.equal(74.24633936261826, result.dFasts[result.dFasts.length - 1]);
  });

  it('calculateStochasticOscillator', function () {
    let candles = [{
      highestPrice: 127.01,
      lowestPrice: 125.36
    }, {
      highestPrice: 127.62,
      lowestPrice: 126.16
    }, {
      highestPrice: 126.59,
      lowestPrice: 124.93
    }, {
      highestPrice: 127.35,
      lowestPrice: 126.09
    }, {
      highestPrice: 128.17,
      lowestPrice: 126.82
    }, {
      highestPrice: 128.43,
      lowestPrice: 126.48
    }, {
      highestPrice: 127.37,
      lowestPrice: 126.03
    }, {
      highestPrice: 126.42,
      lowestPrice: 124.83
    }, {
      highestPrice: 126.90,
      lowestPrice: 126.39
    }, {
      highestPrice: 126.85,
      lowestPrice: 125.72
    }, {
      highestPrice: 125.65,
      lowestPrice: 124.56
    }, {
      highestPrice: 125.72,
      lowestPrice: 124.57
    }, {
      highestPrice: 127.16,
      lowestPrice: 125.07
    }, {
      highestPrice: 127.72,
      lowestPrice: 126.86,
      closingPrice: 127.29
    }, {
      highestPrice: 127.69,
      lowestPrice: 126.63,
      closingPrice: 127.18
    }, {
      highestPrice: 128.22,
      lowestPrice: 126.80,
      closingPrice: 128.01
    }, {
      highestPrice: 128.27,
      lowestPrice: 126.71,
      closingPrice: 127.11
    }, {
      highestPrice: 128.09,
      lowestPrice: 126.80,
      closingPrice: 127.73
    }];

    let result = myTaLib.calculateStochasticOscillator(candles, 14, 3, 3);
    assert.equal(75.79672695951754, result.kFulls[result.kFulls.length - 3]);
    assert.equal(74.24633936261826, result.kFulls[result.kFulls.length - 2]);
    assert.equal(78.98363479758808, result.kFulls[result.kFulls.length - 1]);
    assert.equal(76.34223370657463, result.dFulls[result.dFulls.length - 1]);
  });

  it('calculateStochasticOscillatorWithPastResults', function () {
    let candles = [{
      highestPrice: 127.01,
      lowestPrice: 125.36
    }, {
      highestPrice: 127.62,
      lowestPrice: 126.16
    }, {
      highestPrice: 126.59,
      lowestPrice: 124.93
    }, {
      highestPrice: 127.35,
      lowestPrice: 126.09
    }, {
      highestPrice: 128.17,
      lowestPrice: 126.82
    }, {
      highestPrice: 128.43,
      lowestPrice: 126.48
    }, {
      highestPrice: 127.37,
      lowestPrice: 126.03
    }, {
      highestPrice: 126.42,
      lowestPrice: 124.83
    }, {
      highestPrice: 126.90,
      lowestPrice: 126.39
    }, {
      highestPrice: 126.85,
      lowestPrice: 125.72
    }, {
      highestPrice: 125.65,
      lowestPrice: 124.56
    }, {
      highestPrice: 125.72,
      lowestPrice: 124.57
    }, {
      highestPrice: 127.16,
      lowestPrice: 125.07
    }, {
      highestPrice: 127.72,
      lowestPrice: 126.86,
      closingPrice: 127.29
    }, {
      highestPrice: 127.69,
      lowestPrice: 126.63,
      closingPrice: 127.18
    }, {
      highestPrice: 128.22,
      lowestPrice: 126.80,
      closingPrice: 128.01
    }, {
      highestPrice: 128.27,
      lowestPrice: 126.71,
      closingPrice: 127.11
    }, {
      highestPrice: 128.09,
      lowestPrice: 126.80,
      closingPrice: 127.73
    }, {
      highestPrice: 128.27,
      lowestPrice: 126.13,
      closingPrice: 127.06
    }];

    const pastResults = {
      kFulls: [75.79672695951754, 74.24633936261826, 78.98363479758808],
      dFulls: [76.34223370657463],
      dFasts: [75.79672695951754, 74.24633936261826, 78.98363479758808],
      kFasts: [70.54263565891475,
        67.70025839793286,
        89.14728682170502,
        65.89147286821691,
        81.91214470284233
      ]
    };

    let result = myTaLib.calculateStochasticOscillator(candles, 14, 3, 3, pastResults);

    assert.equal(75.79672695951754, result.kFulls[result.kFulls.length - 4]);
    assert.equal(74.24633936261826, result.kFulls[result.kFulls.length - 3]);
    assert.equal(78.98363479758808, result.kFulls[result.kFulls.length - 2]);
    assert.equal(70.80103359173118, result.kFulls[result.kFulls.length - 1]);
    assert.equal(76.34223370657463, result.dFulls[result.dFulls.length - 2]);
    assert.equal(74.67700258397917, result.dFulls[result.dFulls.length - 1]);
  });


  it('calculateAo', function () {
    let candles = [{
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 1,
      lowestPrice: 1,
    }, {
      highestPrice: 2,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 4,
      lowestPrice: 1,
    }, {
      highestPrice: 5,
      lowestPrice: 1,
    }];

    let ao = myTaLib.calculateAwesomeOscillator(candles);

    assert.equal(0, ao.aos[ao.aos.length - 1]);
  });

  it('calculateAoWithPastResults', function () {
    const pastResults = {
      midPointPrices: [2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        1,
        1.5,
        2,
        2.5,
        3
      ],
      aos: [0]
    };

    let candles = [{
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 1,
      lowestPrice: 1,
    }, {
      highestPrice: 2,
      lowestPrice: 1,
    }, {
      highestPrice: 3,
      lowestPrice: 1,
    }, {
      highestPrice: 4,
      lowestPrice: 1,
    }, {
      highestPrice: 5,
      lowestPrice: 1,
    }, {
      highestPrice: 6,
      lowestPrice: 1,
    }];

    let ao = myTaLib.calculateAwesomeOscillator(candles, pastResults);
    assert.equal(0.4558823529411766, ao.aos[ao.aos.length - 1]);
  });

  it('calculatePsar', function () {
    let candles = [{
      openingPrice: 3755.74,
      highestPrice: 3836.86,
      lowestPrice: 3643.25,
      closingPrice: 3790.55,
    }, {
      openingPrice: 3766.57,
      highestPrice: 3766.57,
      lowestPrice: 3542.73,
      closingPrice: 3546.20,
    }, {
      openingPrice: 3543.12,
      highestPrice: 3576.17,
      lowestPrice: 3371.75,
      closingPrice: 3570.31,
    }, {
      openingPrice: 3488.31,
      highestPrice: 3513.55,
      lowestPrice: 3334.02,
      closingPrice: 3340.81,
    }, {
      openingPrice: 3337.26,
      highestPrice: 3529.75,
      lowestPrice: 3314.75,
      closingPrice: 3529.60,
    }, {
      openingPrice: 3558.21,
      highestPrice: 3756.17,
      lowestPrice: 3558.21,
      closingPrice: 3717.41,
    }, {
      openingPrice: 3715.22,
      highestPrice: 3717.17,
      lowestPrice: 3517.79,
      closingPrice: 3544.35,
    }];

    let psar = myTaLib.calculateParabolicStopAndReverse(candles, 0.02, 0.02, 0.2);
    let expected = {
      ep: 3756.17,
      accFactor: 0.02,
      eqInPsar: -8.828400000000002,
      initialPsar: 3314.75,
      psar: 3314.75,
      trend: 'bullish',
    };

    assert.equal(expected.ep, psar[psar.length - 1].ep);
    assert.equal(expected.accFactor, psar[psar.length - 1].accFactor);
    assert.equal(expected.eqInPsar, psar[psar.length - 1].eqInPsar);
    assert.equal(expected.initialPsar, psar[psar.length - 1].initialPsar);
    assert.equal(expected.psar, psar[psar.length - 1].psar);
    assert.equal(expected.trend, psar[psar.length - 1].trend);
  });

  it('calculatePsarWithPastResults', function () {
    let candles = [{
      openingPrice: 3755.74,
      highestPrice: 3836.86,
      lowestPrice: 3643.25,
      closingPrice: 3790.55,
    }, {
      openingPrice: 3766.57,
      highestPrice: 3766.57,
      lowestPrice: 3542.73,
      closingPrice: 3546.20,
    }, {
      openingPrice: 3543.12,
      highestPrice: 3576.17,
      lowestPrice: 3371.75,
      closingPrice: 3570.31,
    }, {
      openingPrice: 3488.31,
      highestPrice: 3513.55,
      lowestPrice: 3334.02,
      closingPrice: 3340.81,
    }, {
      openingPrice: 3337.26,
      highestPrice: 3529.75,
      lowestPrice: 3314.75,
      closingPrice: 3529.60,
    }, {
      openingPrice: 3558.21,
      highestPrice: 3756.17,
      lowestPrice: 3558.21,
      closingPrice: 3717.41,
    }, {
      openingPrice: 3715.22,
      highestPrice: 3717.17,
      lowestPrice: 3517.79,
      closingPrice: 3544.35,
    }];

    let expected = {
      ep: 3756.17,
      accFactor: 0.02,
      eqInPsar: -8.828400000000002,
      initialPsar: 3314.75,
      psar: 3314.75,
      trend: 'bullish',
    };

    const pastResults = [{
      ep: 3643.25,
      psar: 3836.86,
      trend: 'bearish',
      accFactor: 0.02,
      eqInPsar: 3.8722000000000025
    }, {
      initialPsar: 3832.9878000000003,
      psar: 3832.9878000000003,
      trend: 'bearish',
      ep: 3542.73,
      accFactor: 0.04,
      eqInPsar: 11.610312000000013
    }, {
      initialPsar: 3836.86,
      psar: 3836.86,
      trend: 'bearish',
      ep: 3371.75,
      accFactor: 0.06,
      eqInPsar: 27.906600000000008
    }, {
      initialPsar: 3808.9534000000003,
      psar: 3808.9534000000003,
      trend: 'bearish',
      ep: 3334.02,
      accFactor: 0.08,
      eqInPsar: 37.99467200000003
    }, {
      initialPsar: 3770.9587280000005,
      psar: 3770.9587280000005,
      trend: 'bearish',
      ep: 3314.75,
      accFactor: 0.1,
      eqInPsar: 45.62087280000006
    }, {
      initialPsar: 3725.3378552000004,
      psar: 3314.75,
      trend: 'bullish',
      ep: 3756.17,
      accFactor: 0.02,
      eqInPsar: -8.828400000000002
    }];

    let psar = myTaLib.calculateParabolicStopAndReverse(candles, 0.02, 0.02, 0.2, pastResults);

    assert.equal(expected.ep, psar[psar.length - 1].ep);
    assert.equal(expected.accFactor, psar[psar.length - 1].accFactor);
    assert.equal(expected.eqInPsar, psar[psar.length - 1].eqInPsar);
    assert.equal(expected.initialPsar, psar[psar.length - 1].initialPsar);
    assert.equal(expected.psar, psar[psar.length - 1].psar);
    assert.equal(expected.trend, psar[psar.length - 1].trend);
  });

  it('calculateAtr', function () {
    let candles = [{
      highestPrice: 48.70,
      lowestPrice: 47.79,
      closingPrice: 48.16
    }, {
      highestPrice: 48.72,
      lowestPrice: 48.14,
      closingPrice: 48.61
    }, {
      highestPrice: 48.90,
      lowestPrice: 48.39,
      closingPrice: 48.75
    }, {
      highestPrice: 48.87,
      lowestPrice: 48.37,
      closingPrice: 48.63
    }, {
      highestPrice: 48.82,
      lowestPrice: 48.24,
      closingPrice: 48.74
    }, {
      highestPrice: 49.05,
      lowestPrice: 48.64,
      closingPrice: 49.03
    }, {
      highestPrice: 49.20,
      lowestPrice: 48.94,
      closingPrice: 49.07
    }, {
      highestPrice: 49.35,
      lowestPrice: 48.86,
      closingPrice: 49.32
    }, {
      highestPrice: 49.92,
      lowestPrice: 49.50,
      closingPrice: 49.91
    }, {
      highestPrice: 50.19,
      lowestPrice: 49.87,
      closingPrice: 50.13
    }, {
      highestPrice: 50.12,
      lowestPrice: 49.20,
      closingPrice: 49.53
    }, {
      highestPrice: 49.66,
      lowestPrice: 48.90,
      closingPrice: 49.50
    }, {
      highestPrice: 49.88,
      lowestPrice: 49.43,
      closingPrice: 49.75
    }, {
      highestPrice: 50.19,
      lowestPrice: 49.73,
      closingPrice: 50.03
    }];

    let atr = myTaLib.calculateAverageTrueRange(candles, 14);
    assert.equal(0.5542857142857146, atr);
  });

  it('calculateAtr2', function () {
    let candles = [{
      highestPrice: 48.70,
      lowestPrice: 47.79,
      closingPrice: 48.16
    }, {
      highestPrice: 48.72,
      lowestPrice: 48.14,
      closingPrice: 48.61
    }, {
      highestPrice: 48.90,
      lowestPrice: 48.39,
      closingPrice: 48.75
    }, {
      highestPrice: 48.87,
      lowestPrice: 48.37,
      closingPrice: 48.63
    }, {
      highestPrice: 48.82,
      lowestPrice: 48.24,
      closingPrice: 48.74
    }, {
      highestPrice: 49.05,
      lowestPrice: 48.64,
      closingPrice: 49.03
    }, {
      highestPrice: 49.20,
      lowestPrice: 48.94,
      closingPrice: 49.07
    }, {
      highestPrice: 49.35,
      lowestPrice: 48.86,
      closingPrice: 49.32
    }, {
      highestPrice: 49.92,
      lowestPrice: 49.50,
      closingPrice: 49.91
    }, {
      highestPrice: 50.19,
      lowestPrice: 49.87,
      closingPrice: 50.13
    }, {
      highestPrice: 50.12,
      lowestPrice: 49.20,
      closingPrice: 49.53
    }, {
      highestPrice: 49.66,
      lowestPrice: 48.90,
      closingPrice: 49.50
    }, {
      highestPrice: 49.88,
      lowestPrice: 49.43,
      closingPrice: 49.75
    }, {
      highestPrice: 50.19,
      lowestPrice: 49.73,
      closingPrice: 50.03
    }, {
      highestPrice: 50.36,
      lowestPrice: 49.26,
      closingPrice: 50.31
    }, {
      highestPrice: 50.57,
      lowestPrice: 50.09,
      closingPrice: 50.52
    }, {
      highestPrice: 50.65,
      lowestPrice: 50.30,
      closingPrice: 50.41
    }, {
      highestPrice: 50.43,
      lowestPrice: 49.21,
      closingPrice: 49.34
    }, {
      highestPrice: 49.63,
      lowestPrice: 48.98,
      closingPrice: 49.37
    }, {
      highestPrice: 50.33,
      lowestPrice: 49.61,
      closingPrice: 50.23
    }, {
      highestPrice: 50.29,
      lowestPrice: 49.20,
      closingPrice: 49.24
    }, {
      highestPrice: 50.17,
      lowestPrice: 49.43,
      closingPrice: 49.93
    }, {
      highestPrice: 49.32,
      lowestPrice: 48.08,
      closingPrice: 48.43
    }, {
      highestPrice: 48.50,
      lowestPrice: 47.64,
      closingPrice: 48.18
    }, {
      highestPrice: 48.32,
      lowestPrice: 41.55,
      closingPrice: 46.57
    }, {
      highestPrice: 46.80,
      lowestPrice: 44.28,
      closingPrice: 45.41
    }, {
      highestPrice: 47.80,
      lowestPrice: 47.31,
      closingPrice: 47.77
    }, {
      highestPrice: 48.39,
      lowestPrice: 47.20,
      closingPrice: 47.72
    }, {
      highestPrice: 48.66,
      lowestPrice: 47.90,
      closingPrice: 48.62
    }, {
      highestPrice: 48.79,
      lowestPrice: 47.73,
      closingPrice: 47.85
    }, ];

    let atr = myTaLib.calculateAverageTrueRange(candles, 14);
    assert.equal(1.3163427906911962, atr);
  });

  it('calculateAdx', function () {
    let candles = [{
      highestPrice: 30.1983,
      lowestPrice: 29.4072,
      closingPrice: 29.872,
    }, {
      highestPrice: 30.2776,
      lowestPrice: 29.3182,
      closingPrice: 30.2381,
    }, {
      highestPrice: 30.4458,
      lowestPrice: 29.9611,
      closingPrice: 30.0996,
    }, {
      highestPrice: 29.3478,
      lowestPrice: 28.7443,
      closingPrice: 28.9028,
    }, {
      highestPrice: 29.3477,
      lowestPrice: 28.5566,
      closingPrice: 28.9225,
    }, {
      highestPrice: 29.2886,
      lowestPrice: 28.4081,
      closingPrice: 28.4775,
    }, {
      highestPrice: 28.8334,
      lowestPrice: 28.0818,
      closingPrice: 28.5566,
    }, {
      highestPrice: 28.7346,
      lowestPrice: 27.4289,
      closingPrice: 27.5576,
    }, {
      highestPrice: 28.6654,
      lowestPrice: 27.6565,
      closingPrice: 28.4675,
    }, {
      highestPrice: 28.8532,
      lowestPrice: 27.8345,
      closingPrice: 28.2796,
    }, {
      highestPrice: 28.6356,
      lowestPrice: 27.3992,
      closingPrice: 27.4882,
    }, {
      highestPrice: 27.6761,
      lowestPrice: 27.0927,
      closingPrice: 27.231,
    }, {
      highestPrice: 27.2112,
      lowestPrice: 26.1826,
      closingPrice: 26.3507,
    }, {
      highestPrice: 26.8651,
      lowestPrice: 26.1332,
      closingPrice: 26.3309,
    }, {
      highestPrice: 27.409,
      lowestPrice: 26.6277,
      closingPrice: 27.0333,
    }, {
      highestPrice: 26.9441,
      lowestPrice: 26.1332,
      closingPrice: 26.2221,
    }, {
      highestPrice: 26.5189,
      lowestPrice: 25.4307,
      closingPrice: 26.0144,
    }, {
      highestPrice: 26.5189,
      lowestPrice: 25.3518,
      closingPrice: 25.4605,
    }, {
      highestPrice: 27.0927,
      lowestPrice: 25.876,
      closingPrice: 27.0333,
    }, {
      highestPrice: 27.686,
      lowestPrice: 26.964,
      closingPrice: 27.4487,
    }, {
      highestPrice: 28.4477,
      lowestPrice: 27.1421,
      closingPrice: 28.3586,
    }, {
      highestPrice: 28.5267,
      lowestPrice: 28.0123,
      closingPrice: 28.4278,
    }, {
      highestPrice: 28.6654,
      lowestPrice: 27.884,
      closingPrice: 27.953,
    }, {
      highestPrice: 29.0116,
      lowestPrice: 27.9928,
      closingPrice: 29.0116,
    }, {
      highestPrice: 29.872,
      lowestPrice: 28.7643,
      closingPrice: 29.3776,
    }, {
      highestPrice: 29.8028,
      lowestPrice: 29.1402,
      closingPrice: 29.3576,
    }, {
      highestPrice: 29.7529,
      lowestPrice: 28.7127,
      closingPrice: 28.9107,
    }, {
      highestPrice: 30.6546,
      lowestPrice: 28.929,
      closingPrice: 30.6149,
    }, {
      highestPrice: 30.5951,
      lowestPrice: 30.0304,
      closingPrice: 30.0502,
    }];
    let result = myTaLib.calculateAverageDirectionalMovementIndex(candles).adx;
    assert.equal(32.15349468785428, result);
  });

  it('calculateCci', function () {
    let candles = [{
      highestPrice: 24.20,
      lowestPrice: 23.85,
      closingPrice: 23.89,
    }, {
      highestPrice: 24.07,
      lowestPrice: 23.72,
      closingPrice: 23.95,
    }, {
      highestPrice: 24.04,
      lowestPrice: 23.64,
      closingPrice: 23.67,
    }, {
      highestPrice: 23.87,
      lowestPrice: 23.37,
      closingPrice: 23.78,
    }, {
      highestPrice: 23.67,
      lowestPrice: 23.46,
      closingPrice: 23.50,
    }, {
      highestPrice: 23.59,
      lowestPrice: 23.18,
      closingPrice: 23.32,
    }, {
      highestPrice: 23.80,
      lowestPrice: 23.40,
      closingPrice: 23.75,
    }, {
      highestPrice: 23.80,
      lowestPrice: 23.57,
      closingPrice: 23.79,
    }, {
      highestPrice: 24.30,
      lowestPrice: 24.05,
      closingPrice: 24.14,
    }, {
      highestPrice: 24.15,
      lowestPrice: 23.77,
      closingPrice: 23.81,
    }, {
      highestPrice: 24.05,
      lowestPrice: 23.60,
      closingPrice: 23.78,
    }, {
      highestPrice: 24.06,
      lowestPrice: 23.84,
      closingPrice: 23.86,
    }, {
      highestPrice: 23.88,
      lowestPrice: 23.64,
      closingPrice: 23.70,
    }, {
      highestPrice: 25.14,
      lowestPrice: 23.94,
      closingPrice: 24.96,
    }, {
      highestPrice: 25.20,
      lowestPrice: 24.74,
      closingPrice: 24.88,
    }, {
      highestPrice: 25.07,
      lowestPrice: 24.77,
      closingPrice: 24.96,
    }, {
      highestPrice: 25.22,
      lowestPrice: 24.90,
      closingPrice: 25.18,
    }, {
      highestPrice: 25.37,
      lowestPrice: 24.93,
      closingPrice: 25.07,
    }, {
      highestPrice: 25.36,
      lowestPrice: 24.96,
      closingPrice: 25.27,
    }, {
      highestPrice: 25.26,
      lowestPrice: 24.93,
      closingPrice: 25.00,
    }, {
      highestPrice: 24.82,
      lowestPrice: 24.21,
      closingPrice: 24.46,
    }];

    let result = myTaLib.calculateCommodityChannelIndex(candles, 20);
    assert.equal(102.19852632840085, result[result.length - 2]);
    assert.equal(30.770139381053642, result[result.length - 1]);
  });

  it('calculateMacd', function () {
    let candles = [{
      closingPrice: 459.99
    }, {
      closingPrice: 448.85
    }, {
      closingPrice: 446.06
    }, {
      closingPrice: 450.81
    }, {
      closingPrice: 442.8
    }, {
      closingPrice: 448.97
    }, {
      closingPrice: 444.57
    }, {
      closingPrice: 441.4
    }, {
      closingPrice: 430.47
    }, {
      closingPrice: 420.05
    }, {
      closingPrice: 431.14
    }, {
      closingPrice: 425.66
    }, {
      closingPrice: 430.58
    }, {
      closingPrice: 431.72
    }, {
      closingPrice: 437.87
    }, {
      closingPrice: 428.43
    }, {
      closingPrice: 428.35
    }, {
      closingPrice: 432.5
    }, {
      closingPrice: 443.66
    }, {
      closingPrice: 455.72
    }, {
      closingPrice: 454.49
    }, {
      closingPrice: 452.08
    }, {
      closingPrice: 452.73
    }, {
      closingPrice: 461.91
    }, {
      closingPrice: 463.58
    }, {
      closingPrice: 461.14
    }, {
      closingPrice: 452.08
    }, {
      closingPrice: 442.66
    }, {
      closingPrice: 428.91
    }, {
      closingPrice: 429.79
    }, {
      closingPrice: 431.99
    }, {
      closingPrice: 427.72
    }, {
      closingPrice: 423.2
    }, {
      closingPrice: 426.21
    }, {
      closingPrice: 426.98
    }, {
      closingPrice: 435.69
    }, {
      closingPrice: 434.33
    }, {
      closingPrice: 429.8
    }, {
      closingPrice: 419.85
    }, {
      closingPrice: 426.24
    }, {
      closingPrice: 402.8
    }, {
      closingPrice: 392.05
    }, {
      closingPrice: 390.53
    }, {
      closingPrice: 398.67
    }, {
      closingPrice: 406.13
    }, {
      closingPrice: 405.46
    }, {
      closingPrice: 408.38
    }, {
      closingPrice: 417.2
    }, {
      closingPrice: 430.12
    }, {
      closingPrice: 442.78
    }, {
      closingPrice: 439.29
    }, {
      closingPrice: 445.52
    }, {
      closingPrice: 449.98
    }, {
      closingPrice: 460.71
    }, {
      closingPrice: 458.66
    }, {
      closingPrice: 463.84
    }, {
      closingPrice: 456.77
    }, {
      closingPrice: 452.97
    }, {
      closingPrice: 454.74
    }, {
      closingPrice: 443.86
    }, {
      closingPrice: 428.85
    }, {
      closingPrice: 434.58
    }, {
      closingPrice: 433.26
    }, {
      closingPrice: 442.93
    }, {
      closingPrice: 439.66
    }, {
      closingPrice: 441.35
    }];

    let result = myTaLib.getMovingAverageConvergenceDivergenceSignal(candles, 12, 26, 9, 'closingPrice');
    assert.equal(2.762561215825258, result.macds[result.macds.length - 1]);
    assert.equal(3.9605231415990114, result.signals[result.signals.length - 1]);
    assert.equal(2.9566628561152584, result.macds[result.macds.length - 2]);
    assert.equal(4.2600136230424495, result.signals[result.signals.length - 2]);
  });

  it('calculateMacdWithPastResults', function () {
    const pastResults = {
      fastEmas: [440.8975000000001,
        439.31019230769243,
        438.1424704142013,
        438.10055188893955,
        436.61277467525656,
        435.341578571371,
        434.9044126373139,
        436.25142607772716,
        439.2465912965383,
        441.5917310970709,
        443.20531092829077,
        444.67064770855376,
        447.32285575339165,
        449.8239548682545,
        451.564884888523,
        451.64413336721174,
        450.26195900302537,
        446.97704223332914,
        444.33288188974007,
        442.43397698362617,
        440.1702882169144,
        437.5594746450814,
        435.81340162276115,
        434.454416757721,
        434.64450648730235,
        434.5961208738712,
        433.85825612404483,
        431.7031397972687,
        430.86265675153504,
        426.54532494360654,
        421.2383518753594,
        416.513990048381,
        413.76876081016854,
        412.59356683937335,
        411.4960950179313,
        411.0166957844034,
        411.9679733560336,
        414.76059283972074,
        419.0712708643791,
        422.18184457755154,
        425.772330027159,
        429.4965869460576,
        434.29865049281796,
        438.0465504169998,
        442.0147734297691,
        444.28480828672764,
        445.6209916272311,
        447.02391599227246,
        446.53715968576904,
        443.8160581956507,
        442.3951261655506,
        440.98972214008126,
        441.2882264262226,
        441.03773005295756,
      ],
      slowEmas: [443.2896153846154,
        443.940754985755,
        443.8458842460695,
        442.7395224500643,
        441.78029856487433,
        441.0550912637725,
        440.0673067257153,
        438.81787659788455,
        437.88395981285606,
        437.0762590859779,
        436.9735732277573,
        436.7777529886642,
        436.26088239691126,
        435.04526147862157,
        434.3930198876126,
        432.05279619223387,
        429.08962610392024,
        426.2333575036298,
        424.19162731817573,
        422.8537289983108,
        421.5653046280656,
        420.588615396357,
        420.3376068484787,
        421.06222856340617,
        422.67095237352424,
        423.9019929384484,
        425.5033267948596,
        427.31641369894413,
        429.7900126842075,
        431.9285302631551,
        434.2923428362547,
        435.9573544780136,
        437.2175504426052,
        438.51550966907894,
        438.9113978417398,
        438.16610911272204,
        437.90047140066855,
        437.55673277839685,
        437.95475257258965,
        438.0810671968423,
      ],
      macds: [7.70337838145673,
        6.416074756955879,
        4.2375197832648155,
        2.5525833248657364,
        1.3788857198536562,
        0.10298149119910249, -1.2584019528031263, -2.070558190094914, -2.6218423282568892, -2.329066740454948, -2.18163211479299, -2.402626272866428, -3.3421216813528645, -3.53036313607754, -5.507471248627326, -7.85127422856084, -9.7193674552488, -10.42286650800719, -10.260162158937476, -10.069209610134294, -9.571919611953604, -8.369633492445075, -6.301635723685422, -3.599681509145171, -1.7201483608968715,
        0.26900323229938294,
        2.180173247113487,
        4.508637808610445,
        6.118020153844725,
        7.722430593514389,
        8.32745380871404,
        8.403441184625876,
        8.508406323193526,
        7.625761844029228,
        5.649949082928686,
        4.494654764882057,
        3.432989361684406,
        3.3334738536329382,
        2.9566628561152584,
      ],
      signals: [2.382807914337235,
        1.38187786581841,
        0.6396889445637386,
        0.07542473269239297, -0.4201854684193712, -1.00457271100607, -1.509730796020364, -2.3092788865417564, -3.4176779549455736, -4.678015855006219, -5.826985985606413, -6.713621220272626, -7.38473889824496, -7.822175040986689, -7.931666731278367, -7.605660529759779, -6.804464725636858, -5.787601452688861, -4.576280515691212, -3.2249897631302726, -1.678264248782129, -0.11900736825675828,
        1.4492802240974711,
        2.824914941020785,
        3.9406201897418036,
        4.854177416432148,
        5.408494301951564,
        5.456785258146988,
        5.264359159494003,
        4.898085199932083,
        4.585162930672254,
        4.259462915760856,
      ]
    };

    let candles = [{
      closingPrice: 459.99
    }, {
      closingPrice: 448.85
    }, {
      closingPrice: 446.06
    }, {
      closingPrice: 450.81
    }, {
      closingPrice: 442.8
    }, {
      closingPrice: 448.97
    }, {
      closingPrice: 444.57
    }, {
      closingPrice: 441.4
    }, {
      closingPrice: 430.47
    }, {
      closingPrice: 420.05
    }, {
      closingPrice: 431.14
    }, {
      closingPrice: 425.66
    }, {
      closingPrice: 430.58
    }, {
      closingPrice: 431.72
    }, {
      closingPrice: 437.87
    }, {
      closingPrice: 428.43
    }, {
      closingPrice: 428.35
    }, {
      closingPrice: 432.5
    }, {
      closingPrice: 443.66
    }, {
      closingPrice: 455.72
    }, {
      closingPrice: 454.49
    }, {
      closingPrice: 452.08
    }, {
      closingPrice: 452.73
    }, {
      closingPrice: 461.91
    }, {
      closingPrice: 463.58
    }, {
      closingPrice: 461.14
    }, {
      closingPrice: 452.08
    }, {
      closingPrice: 442.66
    }, {
      closingPrice: 428.91
    }, {
      closingPrice: 429.79
    }, {
      closingPrice: 431.99
    }, {
      closingPrice: 427.72
    }, {
      closingPrice: 423.2
    }, {
      closingPrice: 426.21
    }, {
      closingPrice: 426.98
    }, {
      closingPrice: 435.69
    }, {
      closingPrice: 434.33
    }, {
      closingPrice: 429.8
    }, {
      closingPrice: 419.85
    }, {
      closingPrice: 426.24
    }, {
      closingPrice: 402.8
    }, {
      closingPrice: 392.05
    }, {
      closingPrice: 390.53
    }, {
      closingPrice: 398.67
    }, {
      closingPrice: 406.13
    }, {
      closingPrice: 405.46
    }, {
      closingPrice: 408.38
    }, {
      closingPrice: 417.2
    }, {
      closingPrice: 430.12
    }, {
      closingPrice: 442.78
    }, {
      closingPrice: 439.29
    }, {
      closingPrice: 445.52
    }, {
      closingPrice: 449.98
    }, {
      closingPrice: 460.71
    }, {
      closingPrice: 458.66
    }, {
      closingPrice: 463.84
    }, {
      closingPrice: 456.77
    }, {
      closingPrice: 452.97
    }, {
      closingPrice: 454.74
    }, {
      closingPrice: 443.86
    }, {
      closingPrice: 428.85
    }, {
      closingPrice: 434.58
    }, {
      closingPrice: 433.26
    }, {
      closingPrice: 442.93
    }, {
      closingPrice: 439.66
    }, {
      closingPrice: 441.35
    }];

    let result = myTaLib.getMovingAverageConvergenceDivergenceSignal(candles, 12, 26, 9, 'closingPrice', pastResults);
    assert.equal(2.762561215825258, result.macds[result.macds.length - 1]);
    assert.equal(3.9605231415990114, result.signals[result.signals.length - 1]);
    assert.equal(2.9566628561152584, result.macds[result.macds.length - 2]);
    assert.equal(4.2600136230424495, result.signals[result.signals.length - 2]);
  });

  it('calculateSma', function () {
    let candles = [{
      closingPrice: 1,
    }, {
      closingPrice: 2,
    }, {
      closingPrice: 3,
    }, {
      closingPrice: 4,
    }, {
      closingPrice: 5,
    }];

    let result = myTaLib.calculateSimpleMovingAverage(candles, 5, 'closingPrice');
    assert.equal(3, result[result.length - 1]);
  });

  it('calculateRsi', function () {
    let candles = [{
      closingPrice: 44.3389,
    }, {
      closingPrice: 44.0902,
    }, {
      closingPrice: 44.1497,
    }, {
      closingPrice: 43.6124,
    }, {
      closingPrice: 44.3278,
    }, {
      closingPrice: 44.8264,
    }, {
      closingPrice: 45.0955,
    }, {
      closingPrice: 45.4245,
    }, {
      closingPrice: 45.8433,
    }, {
      closingPrice: 46.0826,
    }, {
      closingPrice: 45.8931,
    }, {
      closingPrice: 46.0328,
    }, {
      closingPrice: 45.614,
    }, {
      closingPrice: 46.282,
    }, {
      closingPrice: 46.282,
    }, {
      closingPrice: 46.0028,
    }];

    let rsi = myTaLib.calculateRelativeStrengthIndex(candles, 14);
    assert.equal(70.53278948369497, rsi.rsis[rsi.rsis.length - 2]);
    assert.equal(66.31856180517232, rsi.rsis[rsi.rsis.length - 1]);
  });

  it('calculateRsiWithPastResults', function () {
    const candles = [{
      closingPrice: 44.3389,
    }, {
      closingPrice: 44.0902,
    }, {
      closingPrice: 44.1497,
    }, {
      closingPrice: 43.6124,
    }, {
      closingPrice: 44.3278,
    }, {
      closingPrice: 44.8264,
    }, {
      closingPrice: 45.0955,
    }, {
      closingPrice: 45.4245,
    }, {
      closingPrice: 45.8433,
    }, {
      closingPrice: 46.0826,
    }, {
      closingPrice: 45.8931,
    }, {
      closingPrice: 46.0328,
    }, {
      closingPrice: 45.614,
    }, {
      closingPrice: 46.282,
    }, {
      closingPrice: 46.282,
    }, {
      closingPrice: 46.0028,
    }];
    const pastResults = {
      gains: [
        0,
        0.059499999999999886,
        0,
        0.7154000000000025,
        0.49859999999999616,
        0.26910000000000167,
        0.3290000000000006,
        0.4187999999999974,
        0.23930000000000007,
        0,
        0.13970000000000482,
        0,
        0.6679999999999993,
        0
      ],
      losses: [
        0.24869999999999948,
        0,
        0.5373000000000019,
        0,
        0,
        0,
        0,
        0,
        0,
        0.18950000000000244,
        0,
        0.4188000000000045,
        0,
        0
      ],
      rsis: [70.53278948369497],
      avgGains: [0.23838571428571445],
      avgLosses: [0.09959285714285773],
    };

    let rsi = myTaLib.calculateRelativeStrengthIndex(candles, 14, pastResults);
    assert.equal(70.53278948369497, rsi.rsis[rsi.rsis.length - 2]);
    assert.equal(66.31856180517232, rsi.rsis[rsi.rsis.length - 1]);
  });

  it('calculateDC', function () {
    let candles = [{
      highestPrice: 5,
      lowestPrice: 4.99,
    }, {
      highestPrice: 4,
      lowestPrice: 4.98,
    }, {
      highestPrice: 3,
      lowestPrice: 4.97,
    }, {
      highestPrice: 2,
      lowestPrice: 4.96,
    }, {
      highestPrice: 4.99,
      lowestPrice: 4.97,
    }];

    let results = myTaLib.calculateDonchianChannel(candles);
    assert.equal(5, results.upperChannel);
    assert.equal(4.96, results.lowerChannel);
    assert.equal(4.98, results.middleChannel);
  });

  it('bullishPinShouldReturnTrueIfBullishCandleLongLowerShadow', function () {
    let candle = {
      openingPrice: 20.1,
      closingPrice: 21,
      highestPrice: 21,
      lowestPrice: 18,
    };

    let result = myTaLib.isBullishPin(candle);
    assert.equal(true, result);
  });

  it('bullishPinShouldReturnFalseIfBullishCandleShortLowerShadow', function () {
    let candle = {
      openingPrice: 2,
      closingPrice: 2.1,
      highestPrice: 2.1,
      lowestPrice: 1.8,
    };

    let result = myTaLib.isBullishPin(candle);
    assert.equal(false, result);
  });

  it('bullishPinShouldReturnTrueIfBearishCandleLongLowerShadow', function () {
    let candle = {
      openingPrice: 21,
      closingPrice: 20.1,
      highestPrice: 21,
      lowestPrice: 18,
    };

    let result = myTaLib.isBullishPin(candle);
    assert.equal(true, result);
  });

  it('bullishPinShouldReturnFalseIfBearishCandleShortLowerShadow', function () {
    let candle = {
      openingPrice: 2.1,
      closingPrice: 2.0,
      highestPrice: 2.1,
      lowestPrice: 1.9,
    };

    let result = myTaLib.isBullishPin(candle);
    assert.equal(false, result);
  });

  it('bearishPinShouldReturnTrueIfBullishCandleLongUpperShadow', function () {
    let candle = {
      openingPrice: 20,
      closingPrice: 20.9,
      highestPrice: 23,
      lowestPrice: 20,
    };

    let result = myTaLib.isBearishPin(candle);
    assert.equal(true, result);
  });

  it('bearishPinShouldReturnFalseIfBullishCandleShortUpperShadow', function () {
    let candle = {
      openingPrice: 20,
      closingPrice: 21,
      highestPrice: 23,
      lowestPrice: 20,
    };

    let result = myTaLib.isBearishPin(candle);
    assert.equal(false, result);
  });

  it('bearishPinShouldReturnTrueIfBearishCandleLongUpperShadow', function () {
    let candle = {
      openingPrice: 20.9,
      closingPrice: 20,
      highestPrice: 23,
      lowestPrice: 20,
    };

    let result = myTaLib.isBearishPin(candle);
    assert.equal(true, result);
  });

  it('bearishPinShouldReturnFalseIfBearishCandleShortUpperShadow', function () {
    let candle = {
      openingPrice: 2.01,
      closingPrice: 2.0,
      highestPrice: 2.2,
      lowestPrice: 1.9,
    };

    let result = myTaLib.isBearishPin(candle);
    assert.equal(false, result);
  });

  it('bullishTweezerBottomsShouldReturnTrue', function () {
    let candles = [{
      openingPrice: 2.11,
      closingPrice: 1.9,
      highestPrice: 2.2,
      lowestPrice: 1.9,
    }, {
      openingPrice: 1.9,
      closingPrice: 2.11,
      highestPrice: 2.2,
      lowestPrice: 1.9,
    }];

    let result = myTaLib.isBullishTweezerBottoms(candles);
    assert.equal(true, result);
  });

  it('bearishTweezerBottomsShouldReturnTrue', function () {
    let candles = [{
      openingPrice: 1.9,
      closingPrice: 2.11,
      highestPrice: 2.2,
      lowestPrice: 1.9,
    }, {
      openingPrice: 2.11,
      closingPrice: 1.9,
      highestPrice: 2.2,
      lowestPrice: 1.9,
    }];

    let result = myTaLib.isBearishTweezerBottoms(candles);
    assert.equal(true, result);
  });

  it('oneWhiteSoldierShouldReturnTrueIfBullishCandleFollowsBearishCandle', function () {
    let candles = [{
      openingPrice: 2.1,
      closingPrice: 1.9,
      highestPrice: 2.1,
      lowestPrice: 1.9,
    }, {
      closingPrice: 2.2,
      openingPrice: 2.0,
      highestPrice: 2.2,
      lowestPrice: 2.0,
    }];

    let result = myTaLib.isOneWhiteSoldier(candles);
    assert.equal(true, result);
  });

  it('oneWhiteSoldierShouldReturnFalseIfBearishCandleFollowsBullishCandle', function () {
    let candles = [{
      closingPrice: 2.2,
      openingPrice: 2.0,
      highestPrice: 2.2,
      lowestPrice: 2.0,
    }, {
      openingPrice: 2.1,
      closingPrice: 1.9,
      highestPrice: 2.1,
      lowestPrice: 1.9,
    }];

    let result = myTaLib.isOneWhiteSoldier(candles);
    assert.equal(false, result);
  });

  it('oneBlackCrowShouldReturnTrueIfBearishCandleFollowsBullishCandle', function () {
    let candles = [{
      closingPrice: 2.2,
      openingPrice: 2.0,
      highestPrice: 2.2,
      lowestPrice: 2.0,
    }, {
      openingPrice: 2.1,
      closingPrice: 1.9,
      highestPrice: 2.1,
      lowestPrice: 1.9,
    }];

    let result = myTaLib.isOneBlackCrow(candles);
    assert.equal(true, result);
  });

  it('oneBlackCrowShouldReturnFalseIfBullishCandleFollowsBearishCandle', function () {
    let candles = [{
      openingPrice: 2.1,
      closingPrice: 1.9,
      highestPrice: 2.1,
      lowestPrice: 1.9,
    }, {
      closingPrice: 2.2,
      openingPrice: 2.0,
      highestPrice: 2.2,
      lowestPrice: 2.0,
    }];

    let result = myTaLib.isOneBlackCrow(candles);
    assert.equal(false, result);
  });

  it('morningStarShouldReturnTrue', function () {
    let candles = [{
      openingPrice: 2.5,
      closingPrice: 1.9,
      highestPrice: 2.5,
      lowestPrice: 1.9,
    }, {
      openingPrice: 1.8,
      closingPrice: 1.8,
      highestPrice: 1.9,
      lowestPrice: 1.8,
    }, {
      openingPrice: 2.0,
      closingPrice: 2.25,
      highestPrice: 2.25,
      lowestPrice: 2.0,
    }];

    let result = myTaLib.isMorningStar(candles);
    assert.equal(true, result);
  });

  it('morningStarShouldReturnFalse', function () {
    let candles = [{
      openingPrice: 1.8,
      closingPrice: 1.9,
      highestPrice: 2.5,
      lowestPrice: 1.8,
    }, {
      openingPrice: 1.8,
      closingPrice: 1.8,
      highestPrice: 1.9,
      lowestPrice: 1.8,
    }, {
      openingPrice: 2.0,
      closingPrice: 2.25,
      highestPrice: 2.25,
      lowestPrice: 2.0,
    }];

    let result = myTaLib.isMorningStar(candles);
    assert.equal(false, result);
  });

  it('eveningStarShouldReturnTrue', function () {
    let candles = [{
      openingPrice: 1.8,
      closingPrice: 2.8,
      highestPrice: 2.8,
      lowestPrice: 1.8,
    }, {
      openingPrice: 3.0,
      closingPrice: 3.0,
      highestPrice: 3.0,
      lowestPrice: 3.0,
    }, {
      openingPrice: 2.8,
      closingPrice: 2.20,
      highestPrice: 2.8,
      lowestPrice: 2.0,
    }];

    let result = myTaLib.isEveningStar(candles);
    assert.equal(true, result);
  });

  it('eveningStarShouldReturnTrue', function () {
    let candles = [{
      openingPrice: 1.8,
      closingPrice: 2.1,
      highestPrice: 2.1,
      lowestPrice: 1.8,
    }, {
      openingPrice: 3.0,
      closingPrice: 3.0,
      highestPrice: 3.0,
      lowestPrice: 3.0,
    }, {
      openingPrice: 2.8,
      closingPrice: 2.20,
      highestPrice: 2.8,
      lowestPrice: 2.0,
    }];

    let result = myTaLib.isEveningStar(candles);
    assert.equal(false, result);
  });

  it('bullishEngulfingShouldReturnTrue', function () {
    let candles = [{
      openingPrice: 2.1,
      closingPrice: 1.8,
      highestPrice: 2.1,
      lowestPrice: 1.8,
    }, {
      openingPrice: 1.7,
      closingPrice: 2.2,
      highestPrice: 2.2,
      lowestPrice: 1.7,
    }];

    let result = myTaLib.isBullishEngulfing(candles);
    assert.equal(true, result);
  });

  it('bullishEngulfingShouldReturnFalse', function () {
    let candles = [{
      openingPrice: 1.7,
      closingPrice: 2.2,
      highestPrice: 2.2,
      lowestPrice: 1.7,
    }, {
      openingPrice: 2.1,
      closingPrice: 1.8,
      highestPrice: 2.1,
      lowestPrice: 1.8,
    }];

    let result = myTaLib.isBullishEngulfing(candles);
    assert.equal(false, result);
  });

  it('bearishEngulfingShouldReturnTrue', function () {
    let candles = [{
      openingPrice: 1.7,
      closingPrice: 1.8,
      highestPrice: 1.8,
      lowestPrice: 1.7,
    }, {
      openingPrice: 1.9,
      closingPrice: 1.6,
      highestPrice: 1.9,
      lowestPrice: 1.6,
    }];

    let result = myTaLib.isBearishEngulfing(candles);
    assert.equal(true, result);
  });

  it('bearishEngulfingShouldReturnFalse', function () {
    let candles = [{
      openingPrice: 1.9,
      closingPrice: 1.6,
      highestPrice: 1.9,
      lowestPrice: 1.6,
    }, {
      openingPrice: 1.7,
      closingPrice: 1.8,
      highestPrice: 1.8,
      lowestPrice: 1.7,
    }];

    let result = myTaLib.isBearishEngulfing(candles);
    assert.equal(false, result);
  });

  it('calculateFI', function () {
    let candles = [{
      closingPrice: 50.8,
      volume: 20177800
    }, {
      closingPrice: 51.56,
      volume: 22429800
    }, {
      closingPrice: 51.35,
      volume: 17645800
    }, {
      closingPrice: 52.45,
      volume: 18790000
    }, {
      closingPrice: 51.65,
      volume: 14946000
    }, {
      closingPrice: 51.51,
      volume: 13656400
    }, {
      closingPrice: 52.18,
      volume: 16368600
    }, {
      closingPrice: 52.4,
      volume: 16784800
    }, {
      closingPrice: 52.36,
      volume: 20740200
    }, {
      closingPrice: 52.31,
      volume: 28405800
    }, {
      closingPrice: 52.48,
      volume: 18346400
    }, {
      closingPrice: 52.46,
      volume: 20805000
    }, {
      closingPrice: 52.24,
      volume: 20718000
    }, {
      closingPrice: 51.8,
      volume: 17401400
    }, {
      closingPrice: 50.26,
      volume: 57969600
    }];

    let result = myTaLib.calculateForceIndex(candles, 3);
    assert.equal(11336676.666666709, result[0]);
    assert.equal(-47044030.939127594, result[result.length - 1]);
  });

  it('calculateMFI', function () {
    const candles = [{
      highestPrice: 24.63,
      lowestPrice: 24.63,
      closingPrice: 24.63,
      volume: 18730.14
    }, {
      highestPrice: 24.69,
      lowestPrice: 24.69,
      closingPrice: 24.69,
      volume: 12271.74
    }, {
      highestPrice: 24.99,
      lowestPrice: 24.99,
      closingPrice: 24.99,
      volume: 24691.41
    }, {
      highestPrice: 25.36,
      lowestPrice: 25.36,
      closingPrice: 25.36,
      volume: 18357.61
    }, {
      highestPrice: 25.19,
      lowestPrice: 25.19,
      closingPrice: 25.19,
      volume: 22964.08
    }, {
      highestPrice: 25.17,
      lowestPrice: 25.17,
      closingPrice: 25.17,
      volume: 15918.95
    }, {
      highestPrice: 25.01,
      lowestPrice: 25.01,
      closingPrice: 25.01,
      volume: 16067.04
    }, {
      highestPrice: 24.96,
      lowestPrice: 24.96,
      closingPrice: 24.96,
      volume: 16568.49
    }, {
      highestPrice: 25.08,
      lowestPrice: 25.08,
      closingPrice: 25.08,
      volume: 16018.73
    }, {
      highestPrice: 25.25,
      lowestPrice: 25.25,
      closingPrice: 25.25,
      volume: 9773.57
    }, {
      highestPrice: 25.21,
      lowestPrice: 25.21,
      closingPrice: 25.21,
      volume: 22572.71
    }, {
      highestPrice: 25.37,
      lowestPrice: 25.37,
      closingPrice: 25.37,
      volume: 12986.67
    }, {
      highestPrice: 25.61,
      lowestPrice: 25.61,
      closingPrice: 25.61,
      volume: 10906.66
    }, {
      highestPrice: 25.58,
      lowestPrice: 25.58,
      closingPrice: 25.58,
      volume: 5799.26
    }, {
      highestPrice: 25.46,
      lowestPrice: 25.46,
      closingPrice: 25.46,
      volume: 7395
    }, {
      highestPrice: 25.33,
      lowestPrice: 25.33,
      closingPrice: 25.33,
      volume: 5818,
    }, ];

    let result = myTaLib.calculateMoneyFlowIndex(candles, 14);
    assert.equal(49.46369488855674, result[result.length - 2]);
    assert.equal(45.10678816083429, result[result.length - 1]);
  });

  it('calculateVolumeWeightedAveragePrice', function () {
    const candles = [{
      highestPrice: 24.63,
      lowestPrice: 24.63,
      closingPrice: 24.63,
      volume: 100
    }, {
      highestPrice: 24.69,
      lowestPrice: 24.69,
      closingPrice: 24.69,
      volume: 200
    }, {
      highestPrice: 24.99,
      lowestPrice: 24.99,
      closingPrice: 24.99,
      volume: 300
    }, {
      highestPrice: 25.36,
      lowestPrice: 25.36,
      closingPrice: 25.36,
      volume: 400
    }];

    let result = myTaLib.calculateVolumeWeightedAveragePrice(candles);
    assert.equal(25.042, result);
  });

  it('calculateRVI', function () {
    const candles = [{
      openingPrice: 51.23,
      highestPrice: 51.5,
      lowestPrice: 50.77,
      closingPrice: 50.8,
      volume: 20177800
    }, {
      openingPrice: 50.79,
      highestPrice: 51.82,
      lowestPrice: 50.52,
      closingPrice: 51.56,
      volume: 22429800
    }, {
      openingPrice: 51.56,
      highestPrice: 51.85,
      lowestPrice: 51.17,
      closingPrice: 51.35,
      volume: 17645800
    }, {
      openingPrice: 51.86,
      highestPrice: 52.5,
      lowestPrice: 51.86,
      closingPrice: 52.45,
      volume: 18790000
    }, {
      openingPrice: 51.71,
      highestPrice: 51.73,
      lowestPrice: 51.13,
      closingPrice: 51.65,
      volume: 14946000
    }, {
      openingPrice: 51.94,
      highestPrice: 51.97,
      lowestPrice: 51.46,
      closingPrice: 51.51,
      volume: 13656400
    }, {
      openingPrice: 51.83,
      highestPrice: 52.2,
      lowestPrice: 51.56,
      closingPrice: 52.18,
      volume: 16368600
    }, {
      openingPrice: 51.83,
      highestPrice: 52.44,
      lowestPrice: 51.77,
      closingPrice: 52.4,
      volume: 16784800
    }, {
      openingPrice: 52.5,
      highestPrice: 52.7,
      lowestPrice: 52.04,
      closingPrice: 52.36,
      volume: 20740200
    }, {
      openingPrice: 52.39,
      highestPrice: 52.73,
      lowestPrice: 52.26,
      closingPrice: 52.31,
      volume: 28405800
    }, {
      openingPrice: 52.82,
      highestPrice: 52.98,
      lowestPrice: 52.27,
      closingPrice: 52.48,
      volume: 18346400
    }, {
      openingPrice: 52.46,
      highestPrice: 53.08,
      lowestPrice: 52.43,
      closingPrice: 52.46,
      volume: 20805000
    }, {
      openingPrice: 52.56,
      highestPrice: 52.65,
      lowestPrice: 52.01,
      closingPrice: 52.24,
      volume: 20718000
    }, {
      openingPrice: 52.11,
      highestPrice: 52.48,
      lowestPrice: 51.67,
      closingPrice: 51.8,
      volume: 17401400
    }];
    let result = myTaLib.calculateRelativeVigorIndex(candles, 10);
    assert.equal(0.00151517962394776, result.rvis[result.rvis.length - 2]);
    assert.equal(-0.1289439253143248, result.rvis[result.rvis.length - 1]);
    assert.equal(0.043970624249704916, result.signals[result.signals.length - 2]);
    assert.equal(-0.00411459303271747, result.signals[result.signals.length - 1]);
  });

  it('calculateOBV', function () {
    const candles = [{
      closingPrice: 53.26,
      volume: 0
    }, {
      closingPrice: 53.30,
      volume: 8200
    }, {
      closingPrice: 53.32,
      volume: 8100
    }, {
      closingPrice: 53.72,
      volume: 8300
    }, {
      closingPrice: 54.19,
      volume: 8900
    }, {
      closingPrice: 53.92,
      volume: 9200
    }];

    let result = myTaLib.calculateOnBalanceVolume(candles);
    assert.equal(24300, result[result.length - 1]);
  });

  it('calculateROC', function () {
    const candles = [{
      closingPrice: 11045.27
    }, {
      closingPrice: 11167.32
    }, {
      closingPrice: 11008.61
    }, {
      closingPrice: 11151.83
    }, {
      closingPrice: 10926.77
    }, {
      closingPrice: 10868.12
    }, {
      closingPrice: 10520.32
    }, {
      closingPrice: 10380.43
    }, {
      closingPrice: 10785.14
    }, {
      closingPrice: 10748.26
    }, {
      closingPrice: 10896.91
    }, {
      closingPrice: 10782.95
    }, {
      closingPrice: 10620.16
    }, {
      closingPrice: 10625.83
    }, {
      closingPrice: 10510.95
    }];

    let result = myTaLib.calculateRateOfChange(candles, 12, 'closingPrice');
    assert.equal(-4.520643387312293, result[result.length - 1]);
    assert.equal(-4.848880483410521, result[result.length - 2]);
    assert.equal(-3.848796815288359, result[result.length - 3]);
  });

  it('calculateTrix', function () {
    const candles = [{
      closingPrice: 102.32
    }, {
      closingPrice: 105.54
    }, {
      closingPrice: 106.59
    }, {
      closingPrice: 107.39
    }, {
      closingPrice: 107.45
    }, {
      closingPrice: 109.08
    }, {
      closingPrice: 109.07
    }, {
      closingPrice: 109.1
    }, {
      closingPrice: 106.09
    }, {
      closingPrice: 106.72
    }, {
      closingPrice: 107.9
    }];

    const trix = myTaLib.calculateTripleExponentialAverage(candles, 3, 3, 'closingPrice');
    assert.equal(-0.04796369078946468, trix.roc[trix.roc.length - 1]);
    assert.equal(2, trix.signals.length);
    assert.equal(0.09175557022599193, trix.signals[trix.signals.length - 1]);
  });

  it('calculateStandardDeviation', function () {
    const values = [2, 4, 4, 4, 5, 5, 7, 9];

    const standardDeviation = myTaLib.calculateStandardDeviation(values);
    assert.equal(2, standardDeviation);
  });

  it('calculateBollingerBands', function () {
    const candles = [{
      closingPrice: 86.16
    }, {
      closingPrice: 89.09
    }, {
      closingPrice: 88.78
    }, {
      closingPrice: 90.32
    }, {
      closingPrice: 89.07
    }, {
      closingPrice: 91.15
    }, {
      closingPrice: 89.44
    }, {
      closingPrice: 89.18
    }, {
      closingPrice: 86.93
    }, {
      closingPrice: 87.68
    }, {
      closingPrice: 86.96
    }, {
      closingPrice: 89.43
    }, {
      closingPrice: 89.32
    }, {
      closingPrice: 88.72
    }, {
      closingPrice: 87.45
    }, {
      closingPrice: 87.26
    }, {
      closingPrice: 89.50
    }, {
      closingPrice: 87.90
    }, {
      closingPrice: 89.13
    }, {
      closingPrice: 90.70
    }];

    const result = myTaLib.calculateBollingerBands(candles, 20, 'closingPrice');
    assert.equal(91.2919107300234, result.upper[result.upper.length - 1]);
    assert.equal(88.70850000000002, result.middle[result.middle.length - 1]);
    assert.equal(86.12508926997663, result.lower[result.lower.length - 1]);
  });
});